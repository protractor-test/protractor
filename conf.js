let HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');

let reporter = new HtmlScreenshotReporter({
	dest: 'output',
	filename: 'report.html'
});

exports.config = {
	baseUrl: 'https://angular.io',
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        browserName: 'chrome',
		chromeOptions: {
        	args: [
        		'--start-fullscreen'
			]
		}
    },
    allScriptsTimeout: 4000,
    getPageTimeout: 4000,
    framework: 'jasmine',
    jasmineNodeOpts: {
        defaultTimeoutInterval: 40000
    },
	// Setup the report before any tests start
	beforeLaunch: function() {
		return new Promise(function(resolve){
			reporter.beforeLaunch(resolve);
		});
	},
	// Close the report after all tests finish
	afterLaunch: function(exitCode) {
		return new Promise(function(resolve){
			reporter.afterLaunch(resolve.bind(this, exitCode));
		});
	},
    onPrepare: function () {
		jasmine.getEnv().addReporter(reporter);

		browser.waitForAngularEnabled(false)
    },
	onComplete      : function() {
		browser.close();
		browser.quit();
	},
	specs: ['tests/search.js']
};
