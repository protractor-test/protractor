const EC = protractor.ExpectedConditions;

class WelcomePage {

	constructor () {
		this.inputSearch = element(by.css('.search-container input'))
		this.blockSearchResults = element(by.css('.search-results'))
		this.blockItemResults = this.blockSearchResults.all(by.css('.search-area'))
		this.btnGetStarted = element(by.css('.homepage-container a'))
	}

	typeSearch (val) {
		browser.wait(EC.visibilityOf(this.inputSearch))
		this.inputSearch.clear()
		this.inputSearch.sendKeys(val);
	}

	followGetStarted () {
		browser.wait(EC.visibilityOf(this.btnGetStarted))
		this.btnGetStarted.click()
	}

	checkSearchResultsPresent () {
		browser.wait(EC.visibilityOf(this.blockSearchResults))
		browser.wait(EC.visibilityOf(this.blockItemResults.first()))

		expect(this.blockItemResults.count()).toBeGreaterThan(0)
	}

	checkSearchResultsNotPresent () {
		browser.wait(EC.not(EC.visibilityOf(this.blockSearchResults)))
	}
}

module.exports = new WelcomePage();
