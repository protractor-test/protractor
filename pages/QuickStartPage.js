const EC = protractor.ExpectedConditions;

class QuickStartPage {

	constructor () {
		this.header = element(by.id('getting-started'))
	}

	checkMainInfo () {
		browser.wait(EC.visibilityOf(this.header))
		browser.wait(EC.urlContains('guide/quickstart'))
	}

}

module.exports = new QuickStartPage();
