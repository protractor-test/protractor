const EC = protractor.ExpectedConditions;

class DocsNavigationPage {

	constructor () {
		this.btnHideNavigation = element(by.css('.mat-toolbar-row .hamburger span'))
		this.blockNavigation = element(by.css('mat-sidenav.mat-sidenav'))
	}

	hideNavigation () {
		browser.wait(EC.visibilityOf(this.btnHideNavigation))
		this.btnHideNavigation.click()
	}

	checkNavigationNotExists () {
		browser.wait(EC.not(EC.visibilityOf(this.blockNavigation)))
	}
}

module.exports = new DocsNavigationPage();
